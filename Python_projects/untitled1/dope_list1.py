# create a set with strings and perform search in set

objects = {"python", "coding", "tips", "for", "beginners"}

# print set.
print(objects)
print(len(objects))

# use of "in" keyword.
if "tips" in objects:
    print("This are the best Python coding tips")

# use of "not in" keywords
if "Java tips" not in objects:
    print("This are the best Python coding tips not in Java")


# add objects into sets
# *** lets initialize an empty set

items = set()
# add 3 strings
items.add("Michael")
items.add("Enoch")
items.add("Raphael")
print(items)
