# test for dynamic typing.

from types import

def CheckIt (x):
    if type(x) == IntType:
        print("You have entered an integer")
    else:
        print("Unable to recognise the input data type")